/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include <math.h>
#include <rc_math.h>
#include <modal_pipe_common.h>

////////////////////////////////////////////////////////////////////////////////
// stuff from common config
////////////////////////////////////////////////////////////////////////////////

// from extrinsics.conf
extern double height_body_above_ground_m;
extern rc_vector_t T_stereo_wrt_body;
extern rc_matrix_t R_stereo_to_body;


/*
 * load the common extrinsics config files
 * this prints out data as it goes
 */
int load_extrinsics_file(void);


////////////////////////////////////////////////////////////////////////////////
// stuff from our own config file
////////////////////////////////////////////////////////////////////////////////

#define VOXL_VISION_OLD_CONF_FILE "/etc/modalai/voxl-vision-px4.conf"
#define VOXL_VISION_HUB_CONF_FILE "/etc/modalai/voxl-vision-hub.conf"

#define OFFBOARD_STRINGS {"off","figure_eight","follow_tag","trajectory"}
typedef enum offboard_mode_t{
	OFF,
	FIGURE_EIGHT,
	FOLLOW_TAG,
	TRAJECTORY
}offboard_mode_t;


#define MAX_VOA_INPUTS 6
#define VOA_FRAME_STRING_LEN 64 // matches voxl_common_config extrinsic frame len
#define VOA_INTPUT_TYPE_STRINGS {"point_cloud","tof","rangefinder"}
#define N_VOA_INPUT_TYPES 3
typedef enum voa_input_type_t{
	VOA_POINT_CLOUD,
	VOA_TOF,
	VOA_RANGEFINDER
}voa_input_type_t;

typedef struct voa_input_t{
	int enabled;
	voa_input_type_t type;
	char input_pipe[MODAL_PIPE_MAX_PATH_LEN];
	char frame[64];
	float max_depth;     // furthest distance the sensor can reliably see
	float min_depth;     // use this to cut off false positives from sensor seeing the props
	float cell_size;     // size of 3d voxel grid cells, increase for more downsampling
	int threshold;  // set to 1 to disable thresholding
	float x_fov_deg;     // FOV of the sensor in the x direction, typically width
	float y_fov_deg;     // FOV of the sensor in the y direction, typically height
	int conf_cutoff; // discard points below this confidence, only applicable to TOF
}voa_input_t;


// these are all the possible parameters from the json config file
// UDP mavlink router
extern int en_localhost_mavlink_udp;
extern int localhost_udp_port_number;
// vio
extern int en_vio;
extern char vio_pipe[MODAL_PIPE_MAX_PATH_LEN];
extern char secondary_vio_pipe[MODAL_PIPE_MAX_PATH_LEN];
extern int en_reset_vio_if_initialized_inverted;
extern float vio_warmup_s;
extern int send_odom_while_failed;
// misc features
extern float  horizon_cal_tolerance;
// APQ8096 only features
extern int    en_reset_px4_on_error;
extern int    en_set_clock_from_gps;
extern int    en_force_onboard_mav1_mode;
// offboard mode
extern offboard_mode_t offboard_mode;
extern int follow_tag_id;
extern int figure_eight_move_home;
extern float robot_radius;
extern double collision_sampling_dt;
extern float max_lookahead_distance;
// fixed frame
extern int en_tag_fixed_frame;
extern int fixed_frame_filter_len;
extern int en_transform_mavlink_pos_setpoints_from_fixed_frame;
// collision prevention (VOA)
extern int en_voa;
extern float voa_upper_bound_m;
extern float voa_lower_bound_m;
extern float voa_memory_s;
extern int   voa_max_pc_per_fusion;
extern float voa_pie_min_dist_m;
extern float voa_pie_max_dist_m;
extern float voa_pie_under_trim_m;
extern int   voa_pie_threshold;
extern float voa_send_rate_hz;
extern int   voa_pie_slices;
extern float voa_pie_bin_depth_m;
// VOA input source configuration
extern int n_voa_inputs;
extern voa_input_t voa_inputs[MAX_VOA_INPUTS];



// load only our own config file without printing the contents
int config_file_load(void);

// prints the current configuration values to the screen.
int config_file_print(void);

int extrinsics_fetch_frame_to_body(char* frame, rc_matrix_t* R, rc_vector_t* T);


#endif // end #define CONFIG_FILE_H
